**Imaging 3.0**
##DWDK\* Model

This repository contains the *model* package, which includes the set of classes used to create the *Multi-Study DICOM Patient-Study Model*.  The  Model represents a single patient with multiple Imaging Studies.  

The *Patient-Study Model* has an information hierarchy with the following relationships:

*  A **Patient** has a set of Attributes and zero or more Studies,

*  A **Study** has a set of Attributes and contains one or more Series,

*  A **Series** has a set of Attributes and contains one or more Instances, and

*  An **Instance** has a set of Attributes and contains images or other data.
  
The class hierarchy includes the **Dataset** class along with the  sub-classes: Patient, Study, Series, and Instance sub-classes.

A **Dataset** contains a set of Attributes that are in increasing ordered by **Tag** number. Patient, Study, Series, and Instance objects are Datasets.

**Attributes** are either simple, in which case they contain a **Tag**, a **Value Representation**, and one or more *values*; or they are Sequences.  A **Sequence** Attribute contains a tag, a value representation of 'SQ', and an ordered set of zero or more Datasets.

<sup>\*</sup>DICOMweb<sup><small>TM</small></sup> Toolkit



