// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:unittest/unittest.dart';
import 'package:core/ie_level.dart';

void main() {
  testIELevels();
}

void testIELevels() {
  print(IELevel.PATIENT);
  test('IELevel:0', () => expect(IELevel.PATIENT_STUDIES.level, 0));
  test('IELevel:1', () => expect(IELevel.PATIENT.level, 1));
  test('IELevel:1', () => expect(IELevel.contains(IELevel.PATIENT_STUDIES, IELevel.PATIENT), true));
  test('IELevel:5', () => expect(IELevel.FRAME.level, 5));
  test('IELevel:6', () => expect(IELevel.ANY.level, 6));
  test('IELevel:1', () => expect(IELevel.contains(IELevel.FRAME, IELevel.ANY), true));
}
