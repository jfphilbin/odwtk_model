Issues:
  
   1) For Patient (or Study, Series, or Instance) attributes should we cache them in a local field or always look them up?
   
 For example,
      
        class Study {
          UID uid;
          
          UID get StudyInstanceUID {
            if (uid != null) {
            return uid;
            } else {
            UID uid = lookup(Tag.StudyInstanceUID);  //lookup signals an error if not found
            return uid;
            }
        }
        
or alternatively,
      
       class Study {      
         UID get StudyInstanceUID {
           return lookup(Tag.StudyInstanceUID);  //lookup signals an error if not found
         }
       }
  