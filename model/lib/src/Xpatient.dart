// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

// An eXtended Patient class with all tags contained in fields.

//TODO this class should be a generated clase that uses tag.patientTags, which will be
//     a List of tags that can be used a the patient level in tag order, so that the
//     following line can be generated:
//       String description = p.lookup(tag.StudyDescription)

class Xpatient extends Patient {


  //Attributes that have been instantiated
  UID           uid;
  DateTime      dateTime;
  Timezone      tz;

  // Attributes that may or may not be instantiated
  String            description;
  DateTime          date;
  PersonName        orderingPhysician;
  PersonName        readingPhysician;
  List<Modality>    acquisitionModalities;
  List<Modality>    reportModalities;
  List<Modality>    otherModalities;
  //TODO this isn't really needed.


  Xpatient(Patient p) : super() {
    //initialize all fields
    //For example:
    description = p.lookup(tag.StudyDescription);
  }





}