// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

//TODO cleanup commented code

class PatientName {
  String prefix;
  String first;
  String middle;
  String last;
  String suffix;

  PatientName(this.prefix, this.first, this.middle, this.last, this.suffix);

  static parse(String s) {

  }

  //TODO fix so it prints w/o double spaces, etc.
  String format({order: #first}) => '$prefix $first $middle $last $suffix';
}

class PatientID {
  String authority;
  String identifier;

  PatientID(this.authority, this.identifier) {

  }
}

class OrganizationIdentifier {
  String NamespaceEntityID;     //0040,0031
  String UniversalEntityID;     // 0040,0032
  String UniversalEntityIDType; //0040,0033 (DNS, EUI65, ISO, URI, UUID, X400, X500)

}

/*
Attribute Name  Tag Type  Attribute Description
Local Namespace Entity ID (0040,0031) 1C  Identifies an entity within the local namespace
or domain. Required if Universal Entity ID (0040,0032) is not present; may be present
otherwise.

Universal Entity ID (0040,0032) 1C  Universal or unique identifier for an entity.
Required if Local Namespace Entity ID (0040,0031) is not present; may be present otherwise.


Universal Entity ID Type  (0040,0033) 1C  Standard defining the format of the Universal
Entity ID. Required if Universal Entity ID (0040,0032) is present.
Defined Terms:
  DNS An Internet dotted name. Either in ASCII or as integers
  EUI64 An IEEE Extended Unique Identifier
  ISO An International Standards Organization Object Identifier
  URI Uniform Resource Identifier
  UUID  The DCE Universal Unique Identifier
  X400  An X.400 MHS identifier
  X500  An X.500 directory name
*/

class Sex {
  final Symbol value;

  const Sex(this.value);

  static const MALE = const Sex(#male);
  static const FEMALE = const Sex(#female);
  static const OTHER = const Sex(#other);

  static Sex toSex(String s) {
    s = s.toLowerCase();
    if (s == "female") {
      return FEMALE;
    } else if (s == "male") {
      return MALE;
    } else if (s == "other") {
      return OTHER;
    }
    //TODO fix
    throw new Error();
  }
}

class TypeOfPatientID {
  final Symbol value;

  const TypeOfPatientID(this.value);

  static const TEXT = const TypeOfPatientID(#text);
  static const RFID = const TypeOfPatientID(#rfid);
  static const BARCODE = const TypeOfPatientID(#barcode);

  static TypeOfPatientID stringToTypeOfPatientID(String s) {
    s = s.toLowerCase();
    if (s == "text") {
      return TEXT;
    } else if (s == "rfid") {
      return RFID;
    } else if (s == 'barcode') {
      return BARCODE;
    }
    //TODO add real error
    throw new Error();
  }
}


class Patient extends Dataset {

  PatientName  _name;
  PatientID    _id;
  DateTime     _birthDate;
  Sex          _sex;
  //OtherIDs     _otherIDs;
  //OtherIDsSeq  _otherIDsSeq;

  //TODO parent is really PatientStudy
  Patient(this._name, this._id, this._birthDate, this._sex) : super(null);

  // Getters & Setters
  PatientName get name => (_name != null) ? _name : _name = lookup(Tag.PatientName);
  PatientID   get id   => (_id != null)   ? _id : _id = lookup(Tag.PatientID);
  Sex         get sex =>  (_sex != null)  ? _sex : _sex = Sex.toSex(lookup(Tag.PatientSex));
  DateTime    get birthDate => (_birthDate != null) ? _birthDate
      : utcDateTime(Tag.PatientBirthDate, Tag.PatientBirthTime);

  //b get refPatientSeq => lookup(Tag.ReferencedPatientSequence);

  //OtherIDs    get otherIDs => (_otherIDs != null) ? _otherIDs : _getValue(0x00101000);
  //OtherIDsSeq get otherIDsSeq => (_otherIDsSeq != null) ? _otherIDs : _getValue(0x00101002);

  // Static Methods
  static bool isPatientLevel(tag) => (tag.group == 0x0010);

  // Methods
  dynamic _getValue(tag) => lookup(tag);
}

/*
Patient's Name  (0010,0010)
2 Patient's full name.
Patient ID  (0010,0020)
2
Include Issuer of Patient ID Macro Table 10-18
----
Attribute Name  Tag Type  Attribute Description
Issuer of Patient ID  (0010,0021) 3 Identifier of the Assigning Authority (system, organization, agency, or department) that issued the Patient ID.
Note: Equivalent to HL7 v2 CX component 4 subcomponent 1.

Issuer of Patient ID Qualifiers Sequence  (0010,0024) 3 Attributes specifying or qualifying the identity of the issuer of the Patient ID, or scoping the Patient ID.
Only a single Item is permitted in this sequence.

>Universal Entity ID  (0040,0032) 3 Universal or unique identifier for the Patient ID Assigning Authority. The authority identified by this attribute shall be the same as that of Issuer of Patient ID (0010,0021), if present.
Note: Equivalent to HL7 v2 CX component 4 subcomponent 2 (Universal ID).

>Universal Entity ID Type (0040,0033) 1C  Standard defining the format of the Universal Entity ID (0040,0032). Required if Universal Entity ID (0040,0032) is present.
Note: Equivalent to HL7 v2 CX component 4 subcomponent 3 (Universal ID Type).
See Section 10.14 for Defined Terms.

>Identifier Type Code (0040,0035) 3 Type of Patient ID. Refer to HL7 v2 Table 0203 for
Defined Terms. Note: Equivalent to HL7 v2 CX component 5 (Identifier Type Code).

>Assigning Facility Sequence  (0040,0036)
3 The place or location identifier where the identifier was first assigned to the patient. This component is not an inherent part of the identifier but rather part of the history of the identifier.
Only a single Item is permitted in this sequence.
Note: Equivalent to HL7 v2 CX component 6 (Assigning Facility).
>>Include HL7v2 Hierarchic Designator Macro Table 10-17
>Assigning Jurisdiction Code Sequence (0040,0039)
3 The geo-political body that assigned the patient identifier. Typically a code for a country or a state/province. Only a single Item is permitted in this sequence.
Note: Equivalent to HL7 v2 CX component 9 (Identifier Type Code).
>>Include ‘Code Sequence Macro’ Table 8.8-1 Baseline CID 5001 for country codes.

>Assigning Agency or Department Code Sequence (0040,003A)
3 The agency or department that assigned the patient identifier. Only a single Item is permitted in this sequence.
Note: Equivalent to HL7 v2 CX component 10 (Identifier Type Code).
>>Include ‘Code Sequence Macro’ Table 8.8-1 No Baseline Context Group.

----
Patient's Birth Date  (0010,0030)
2 Birth date of the patient.
Patient's Sex (0010,0040)
2 Sex of the named patient.
Enumerated Values:
  M = male
  F = female
  O = other
Referenced Patient Sequence (0008,1120)
3 A sequence that provides reference to a Patient SOP Class/Instance pair.
Only a single Item is permitted in this Sequence.
>Include SOP Instance Reference Macro Table 10-11
Patient's Birth Time  (0010,0032)
3 Birth time of the Patient.
Other Patient IDs (0010,1000)
3 Other identification numbers or codes used to identify the patient.
Other Patient IDs Sequence  (0010,1002)
3 A sequence of identification numbers or codes used to identify the patient, which may or may not  be human readable, and may or may not have been obtained from an implanted or attached device such as an RFID or barcode.
One or more Items are permitted in this sequence.
>Patient ID (0010,0020)
1 An identification number or code used to identify the patient.
>Include Issuer of Patient ID Macro Table 10-18
>Type of Patient ID (0010,0022)
1 The type of identifier in this item. Defined Terms:
  TEXT
  RFID
  BARCODE
Note: The identifier is coded as a string regardless of the type, not as a binary value.
Other Patient Names (0010,1001)
3 Other names used to identify the patient.
Ethnic Group  (0010,2160)
3 Ethnic group or race of the patient.
Patient Comments  (0010,4000)
3 User-defined additional information about the patient.
Patient Species Description (0010,2201)
1C  The species of the patient.
Required if the patient is an animal and if Patient Species Code Sequence (0010,2202) is not present. May be present otherwise.
Patient Species Code Sequence (0010,2202)
1C  The species of the patient.
Only a single Item shall be included in this sequence.
Required if the patient is an animal and if Patient Species Description (0010,2201) is not present. May be present otherwise.
>Include ‘Code Sequence Macro’ Table 8.8-1  Defined CID 7454.

Patient Breed Description (0010,2292)
2C  The breed of the patient. See C.7.1.1.1.1.
Required if the patient is an animal and if Patient Breed Code Sequence (0010,2293) is empty. May be present otherwise.
Patient Breed Code Sequence (0010,2293)
2C  The breed of the patient. See C.7.1.1.1.1.
Zero or more Items shall be included in this sequence.
Required if the patient is an animal.
>Include ‘Code Sequence Macro’ Table 8.8-1  Defined CID 7480.

Breed Registration Sequence (0010,2294)
2C  Information identifying an animal within a breed registry.
Zero or more Items shall be included in this sequence.
Required if the patient is an animal.
>Breed Registration Number  (0010,2295)
1 Identification number of an animal within the registry.
>Breed Registry Code Sequence (0010,2296)
1 Identification of the organization with which an animal is registered.
Only a single Item shall be permitted in this sequence.
>>Include ‘Code Sequence Macro’ Table 8.8-1 Defined CID 7481.

Responsible Person  (0010,2297)
2C  Name of person with medical decision making authority for the patient.
Required if the patient is an animal. May be present otherwise.
Responsible Person Role (0010,2298)
1C  Relationship of Responsible Person to the patient.
See Section C.7.1.1.1.2 for Defined Terms.
Required if Responsible Person is present and has a value.
Responsible Organization  (0010,2299)
2C  Name of organization with medical decision making authority for the patient.
Required if patient is an animal. May be present otherwise.
Patient Identity Removed  (0012,0062)
3 The true identity of the patient has been removed from the Attributes and the Pixel Data
Enumerated Values:
  YES
  NO
De-identification Method  (0012,0063)
1C  A description or label of the mechanism or method use to remove the patient’s identity. May be multi-valued if successive de-identification steps have been performed.
Notes:  1. This may be used to describe the extent or thoroughness of the de-identification, for example whether or not the de-identification is for a “Limited Data Set” (as per HIPAA Privacy Rule).
  2. The characteristics of the de-identifying equipment and/or the responsible operator of that equipment may be recorded as an additional item of the Contributing Equipment Sequence (0018,A001) in the SOP Common Module. De-identifying equipment may use a Purpose of Reference of (109104,DCM,“De-identifying Equipment”).
Required if Patient Identity Removed (0012,0062) is present and has a value of YES and De-identification Method Code Sequence (0012,0064) is not present. May be present otherwise.
De-identification Method Code Sequence  (0012,0064)
1C  A code describing the mechanism or method use to remove the patient’s identity.
One or more Items shall be included in this sequence. Multiple items are used if successive de-identification steps have been performed or to describe options of a defined profile.
Required if Patient Identity Removed (0012,0062) is present and has a value of YES and De-identification Method (0012,0063) is not present. May be present otherwise.
>Include Code Sequence Macro Table 8.8-1  Defined CID 7050.
*/