// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

class Patient extends Dataset {
  //TODO should the patient dataset be here or above
  final IELevel level = IELevel.PATIENT;
  FileMeta          fmi;
  Uuid              uuid;
  DateTime          dateTime;

  // Patient fields
  Person            person;
  Map<String, String> mrns;    //<issuer, identifier>

  // Constructor
  Patient(PatientStudies parent, this.fmi) : super();


  Sex getSex(Dataset ds) {
    return new Sex(ds.getValue(TagDef.PatientSex));
  }

  Person getPersonFromDataset(Dataset ds) {
    PersonName name    = ds.getValue(TagDef.PatientName);
    DateTime   dob     = getDoB(ds);
    Sex        sex     = getSex(ds);
    String     address = ds.getValue(TagDef.Patient.address);
    return new Person(name, dob, sex, address);
  }

  void initPatientFromDataset(Dataset ds) {
    Person person = getPersonFromDataset(ds);
    String id     = ds.getValue(TagDef.PatientID);
    String ipid   = ds.getValue(TagDef.IssuerOfPatientID);
  }

  DateTime getDoB(Dataset ds) {
    DateTime date = super.getValue(TagDef.PatientPatientsBirthTime);
    DateTime time = ds.getValue(TagDef.PatientPatientsBirthTime);
    return DcmDateAndTimeToUTC(date, time, Dataset.timezone);
  }

  PatientStudies get patientStudies => super.parent;

  String mrn(String issuer) => mrns[issuer];

  //TODO what to do about this?
  //void addMRN(MRN mrn) { mrns[mrn.issuer] = mrn.id; }

  String deleteMRN(MRN mrn) => mrns.remove(mrn.issuer);

  //TODO finish
  String toPrettyString() => "";

  String toString() => 'Patient: ${person.name}, MRN=$mrns, DOB=${person.dateOfBirth}';

}
