// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

class DSStack {
  List<Dataset> stack = new List();

  void push(Dataset ds) => stack.add(ds);

  Dataset get pop      => stack.removeLast();
  Dataset get top      => (stack.isEmpty) ? null : stack.last;
  int     get length   => stack.length;

  toString() => 'DSStack:$stack';
}

/**
 * A [tag] ordered list of DICOM [Dataset].
 */
//TODO decide if we need this to extend to AbstractDataset?
class Dataset {
  //!! A stack of datasets
  //!! static DSStack  stack = new DSStack();
  // The parent of this Dataset
  Dataset         parent;
  final List<int> tags = [];
  final List<VR>  vrs = [];
  final List      values = [];  //TODO values is a List of values
  // TODO determine if sequence is needed
  Sequence        sequence;  // The sequence (SQ) that contains this dataset if any

  Dataset(this.parent);

  Study get study => parent.study;

  UID get tsuid => parent.tsuid;

  Attribute operator [](index) {
    //TODO Issue: this could return an attribute rather than a list.  Is there a
    //            significant performance difference?
    return new Attribute(tags[index], vrs[index], values[index]);
    //TODO return [tags[index], vrs[index], values[index]];
  }

  void operator []=(index, Attribute a) {
    //TODO what should this be doing?  Insert? Replace?  Is it needed?
    tags[index] = a.tag;
    vrs[index] = a.vr;
    values[index] = a.value;
  }

  int     get tag(int i) => tags[i];
  VR      get vR(int i) => vrs[i];
  dynamic get value(int i) => values[i];

  int indexOf(int tag) {
    Dataset ds = this;
    int i = ds.tags.indexOf(tag);
    if (i != null) {
      return i;
    } else if (parent == null) {
      return null;
    } else {
    return parent.indexOf(tag);
    }
  }

  dynamic lookup(Tag tag) => getValue(indexOf(tag.code));

  //TODO this needs to be recursive
  Attribute lookupAttribute(Attribute a) => getValue(a.tag);

  /// Walks recursively up the tree until it find the root = PatientStudies
  Patient get patient {
    Dataset ds = this;
    while (!(ds is PatientStudies)) ds = ds.parent;
    return ds.patient;
  }

  /// Accessors
  UID get studyUid => lookup(Tag.StudyInstanceUID);
  UID get seriesUid => lookup(Tag.SeriesInstanceUID);
  UID get instanceUid => lookup(Tag.SOPInstanceUID);

  /// Predicates for types of Studies, Series and Instances
  //TODO
  //bool get isImage => getValue(TagDef.TODO);
  //bool get isReport => getValue(TagDef.TODO);
  //bool get isPresentationState => getValue(TagDef.TODO);

  // Assumes tag order
  void addAttribute(Attribute a) => add(a.tag, a.vr, a.value);

  void add(int tag, VR vr, value) {
    tags.add(tag);
    vrs.add(vr);
    values.add(value);
  }

  void insert(int tag, VR vr, value) {
    //TODO test
    bool done = false;
    for (var i = 0; i < tags.length; i++) {
      if (tag == tags[i]) {
        parseError("duplicate tag in Dataset");
      } else if (tag < tags[i]) {
        tags.insert(i, tag);
        vrs.insert(i, vr);
        values.insert(i, value);
        done = true;
        break;
      }
    }
    if (done == false) add(tag, vr, value);
  }

  Attribute remove(tag) {
    Attribute a = null;
    for (var i = 0; i < tags.length; i++) {
      if (i == tag) {
        a = new Attribute(tag, vrs[i], values[i]);
        tags.remove(i);
        vrs.remove(i);
        values.remove(i);
      }
    }
    if (a == null) parseError("in Dataset.removeAttribute");
    return a;
  }

  // Getters for special tags

  DateTime utcDateTime(dateTag, timeTag) {
    Date date = lookup(dateTag);
    Time time = lookup(timeTag);
    return utcDateTime(date, time);
  }


  //TODO finish
  toString() => 'Dataset:';

}
