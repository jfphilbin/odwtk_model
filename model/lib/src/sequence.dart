// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

/**
 * A DICOM [Sequence], which contains a [tag] that specifies the type of [Sequence] and
 * an [List] of [Datasets] contained in [this].
 */
class Sequence {
  Dataset parent;
  int tag;                  // The DICOM [tag] that defines the meaning of this Sequence.
  int length;               // The length (in bytes) of the serialized Sequence.
  List<Dataset> items = [];
  bool isModified = false;

  Sequence(this.parent, this.tag, this.length);

}