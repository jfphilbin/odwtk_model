// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

/*
 * The Patient information from one or more of their DICOM studies.
 */
class Patient extends Dataset {
  final IELevel level = IELevel.PATIENT;
  //Attributes that have names for speed instantiated
  PatientStudies parent;
  MRN mrn;

  Patient(PatientStudies parent) : super(parent);

  List<Study> get studies => parent.studies.values;

  //TODO is this the right way to handle the fact that patients have no children?
  Map get children => const {};
}