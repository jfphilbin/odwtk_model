// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

/// A wrapper for DICOM File Meta Information datasets.

class FileMetaInfo extends Dataset {
  //TODO Is this the right level
  final IELevel level = IELevel.ANY;

  FileMetaInfo(Dataset parent) : super(parent);
}