// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

class DatasetStack {

  List<Dataset> stack = new List();

  void push(Dataset ds) => stack.add(ds);
  Dataset get pop => stack.removeLast();
  Dataset get top => stack.last;


  toString() => 'DatasetStack:$stack';
}