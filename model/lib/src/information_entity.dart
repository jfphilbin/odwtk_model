// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

/*
 * #%L
 * MINT Core Library
 * %%
 * Copyright (C) 2013 - 2014 Johns Hopkins University
 * %%
 * All rights reserved.
 *
 * This source code contains the intellectual property of its copyright
 * holders, and is made available under a license. If you do not know the terms
 * of the license, please review it before you read further.
 *
 * You can read LICENSE.html for detailed information about the license terms
 * this source code file is available under.
 *
 * Questions should be directed to james.philbin@jhmi.edu
 * #L%
 */

abstract class InformationEntity {
  //static const int  defaultNChildren = 8;
  final IELevel     level = IELevel.ANY;

  String toPrettyString();

  String toString();
}