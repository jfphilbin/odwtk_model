// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

class Xstudy extends Study {


  // Attributes that may or may not be instantiated
  String            description;
  DateTime          date;
  PersonName        orderingPhysician;
  PersonName        readingPhysician;
  List<Modality>    acquisitionModalities;
  List<Modality>    reportModalities;
  List<Modality>    otherModalities;
  //TODO this isn't really needed.


}