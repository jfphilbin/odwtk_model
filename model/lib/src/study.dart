// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

class Study extends Dataset {
  //ToDo make sure these names correspond to attribute keywords.
  final IELevel level = IELevel.STUDY;
  Dataset          fmi;
  Dataset          ds;
  Map<UID, Series> series;
  //Attributes that have been instantiated
  UID              uid;
  UID              _tsuid;
  DateTime         _dateTime;
  Duration         timezoneOffset;

  Study(PatientStudies parent): super(parent);

  Study.init(PatientStudies parent, this.fmi) : super(parent);

  void initialize() {
    uid = lookup(Tag.StudyInstanceUID);
  }

  Study get study => this;

  UID get tsuid => (_tsuid != null) ? _tsuid : parent.tsuid;

  // Lazy getters for Date, Time and DateTime
  DateTime get dateTime => ((_dateTime != null))
      ? _dateTime
      : utcDateTime(ds.lookup(Tag.SeriesDate), ds.lookup(Tag.SeriesTime));

  PatientStudies get patientStudies => parent;

  void addSeries(UID uid, Series newSeries) {
    series[uid] = newSeries;
  }
}