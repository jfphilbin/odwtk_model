// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

//TODO cleanup commented code

class PatientStudies extends Dataset {
  final IELevel         level = IELevel.PATIENT_STUDIES;
  final Map<UID, Study> studies = new Map(); // Map of <UID, Study>
        Patient         patient;
        FileMetaInfo    fmi;      //File Meta Info if any
        Uri             uri;      //URI where the study is stored

  PatientStudies(Dataset parent) : super(parent);

  /* flush if not used
  factory PatientStudies.fileMeta(Patient patient, Map<UID, Study> studies, FileMeta fmi, Uri uri) {
    PatientStudies ps = new PatientStudies(patient, studies, fmi, uri);
    return ps;
  }
  */
  Dataset get dataset => patient;

  Study addStudy(Study study) => studies[study.uid] = study;

  Study getStudy(UID uid) => studies[uid];

  String toPrettyString() {
    String s;
    //TODO finish
    return s;
  }

  String toString() => "PatientStudies.(patient=$patient, fileUri=$uri)";
}

