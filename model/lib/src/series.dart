// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

//TODO cleanup commented code

class Series extends Dataset {
  final IELevel level = IELevel.SERIES;
  Map<UID, Instance> instances;
  UID                uid;
  UID                _tsuid;     //Transfer Syntax
  DateTime           _dateTime;

  // Series Fields
  List<Modality> acquisition;
  List<Modality> other;

  Series(Study parent) : super(parent);

  Series.init(Study parent, this.uid, this._tsuid) : super(parent);

  //TODO eliminate this
  void initializa() {
    uid = lookup(Tag.SeriesInstanceUID);
  }

  Study get study => parent.study;

  UID get tsuid => (_tsuid != null) ? _tsuid : parent.tsuid;

  DateTime get dateTime =>
      ((_dateTime != null))
        ? _dateTime
        : utcDateTime(Tag.SeriesDate, Tag.SeriesTime);

  //TODO needed?  used?
  /*
  void addInstance(Instance instance) {
    instances[uid] = instance;
  }
   */

  toPrettyString() {
  }
}