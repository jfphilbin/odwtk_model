// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
part of model;

class Instance extends Dataset  {
  final IELevel level = IELevel.INSTANCE;
  UID      uid;
  UID      _tsuid;     //Transfer Syntax
  DateTime _dateTime; //Creation DateTime

  Instance(Series parent) : super(parent);
  Instance.init(parent, this.uid, this._tsuid) : super(parent);

  Study get study => parent.study;

  UID get tsuid => (_tsuid != null) ? _tsuid : super.tsuid;

  void initialize() {
    uid = lookup(Tag.SOPInstanceUID);
  }

  DateTime get dateTime => (_dateTime != null)
      ? _dateTime
      : utcDateTime(lookup(Tag.SeriesDate), lookup(Tag.SeriesTime));

  toPrettyString() {
  }
}