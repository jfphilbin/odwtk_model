// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library model;

/**
 * The library contains the information model used to instantiate the Patient-Study
 * hierarchy:
 */
import 'dart:core' hide DateTime;

//TODO clean up unnecessary comments
import 'package:core/src/date/date.dart';
import 'package:core/src/date/date_time.dart';
import 'package:core/src/date/time.dart';
import 'package:dictionary/dictionary.dart';


//import 'package:core/src/date/time_zone.dart';

export 'eval_mode.dart';

part 'src/new_patient.dart';
part 'src/patient_studies.dart';
part 'src/patient.dart';
part 'src/study.dart';
part 'src/series.dart';
part 'src/instance.dart';
part 'src/sequence.dart';
part 'src/dataset.dart';
part 'src/file_meta.dart';


