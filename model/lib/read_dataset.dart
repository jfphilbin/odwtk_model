// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library convert.readDataset;

import 'dart:typed_data';

import 'package:dictionary/vr.dart';
//import 'package:model/model.dart';
import 'package:core/core.dart';
import 'package:io/read_buffer.dart';
import 'package:utilities/utilities.dart';


class DatasetReader {
  ReadBuffer rb;

  DatasetReader(this.rb);

  //***** DICOM Attribute Utilities *****
  /// Read the DICOM Attribute Tag
  int readTag() {
    int group = rb.uInt16();
    int element = rb.uInt16();
    return (group << 16) + element;
  }

  /// Peek at next tag - doesn't move the [ReadBuffer.index]
  int peekTag() {
    int group = rb.peek16();
    int element = rb.peek16();
    return (group << 16) + element;
  }

  /// Read a Value Representation (VR).  See PS3.10.
  VR readVR() {
    int c1 = rb.uInt8();
    int c2 = rb.uInt8();
    int vrCode = c2 + (c1 << 8);
    //print(intToHex(vrCode));
    return VR.codeToVR(vrCode);
  }

  /// Read the Value Field Length
  /// Note: This is always an even number, but the last byte might be
  /// padding.  See DICOM PS3.5.
  //TODO fix documentation above
  int readVFLength(bool isLarge) {
    if (isLarge) {
      rb.skip(2);
      return rb.uInt32();
    } else {
      return rb.int16();
    }
  }

  int getVFLimit(int vfLength) => rb.index + vfLength;

  // Utility for debugging
  void printAttribute(tag, vr, value) {
    print('[${tagToHex(tag)}, $vr, $value]');
  }

  /// Read a top level [Dataset]
  Dataset readTopLevel() {
    int tag = readTag();
    VR vr = readVR();
    int vfLength = readVFLength(vr.isLarge);
    int vfLimit = getVFLimit(vfLength);
    var value;
    if (vr == VR.SQ) {
      if (vfLength != -1) {
        return readSequence(null, tag, vfLimit);
      } else {
        return readDelimitedSequence(null, tag);
      }
    } else {
      Dataset ds = new Dataset(null);
      while (rb.isNotEmpty) {
        value = readSimpleValue(tag, vr, vfLimit);
        ds.add(tag, vr, value);
      }
      return ds;
    }
  }

  /** (0006,9902) VR=SQ VM=1 Patient Studies Sequence */
  static const int PatientStudiesSequence = 0x00069902;

  /** (5201,9230) VR=SQ VM=1 Per-series Functional Groups Sequence */
  static const int PerSeriesFunctionalGroupsSequence = 0x52019230;
  static const int SeriesSequence = 0x00069904;

  /** (5203,9230) VR=SQ VM=1 Per-instance Functional Groups Sequence */
  static const int PerInstanceFunctionalGroupsSequence = 0x52039230;
  static const int InstanceSequence = 0x00069906;

  /** (0006,9908) VR=UI VM=1 MediaType */
  static const int MediaType = 0x00069908;

  /**
     * Read Sequence
     *
     * TODO document
     */
  dynamic readSequence(Dataset ds, int tag, int vfLength) {
    int vfLimit = getVFLimit(vfLength);
    var value;
    switch (tag) {
      case PatientStudiesSequence:
        PatientStudies value = readPatientStudiesSequenceItems(ds, vfLimit);
        break;
      case SeriesSequence:
      case PerSeriesFunctionalGroupsSequence:
        Study value = readSeriesSequenceItems(ds, vfLimit);
        break;
      case InstanceSequence:
      case PerInstanceFunctionalGroupsSequence:
        Series value = readInstanceSequenceItems(ds, vfLimit);
        break;
      default:
        Dataset value = readSequenceItems(ds, tag, vfLimit);
    }
    return value;
  }

  PatientStudies readPatientStudiesSequenceItems(Dataset parent, int vfLimit) {
    PatientStudies ps = new PatientStudies(parent);
    readFileMetaInfo(parent, vfLimit);
    readPatient(ps, vfLimit);
    readStudies(ps, vfLimit);
    return ps;
  }

  Dataset readFileMetaInfo(Dataset parent, int vfLength) {
    FileMetaInfo fmi = new FileMetaInfo(parent);

  }

  Dataset readPatient(PatientStudies ps, vfLimit) {
    // Read Item tag and lenght
    int vfLength = readItemLength();
    int vfLimit = getVFLimit(vfLength);
    Patient patient = new Patient(ps);
    ps.patient = readItem(patient);
    //TODO what to do about patient id and other fields
    // patient.id
    // patient.ipid
    // patient.name
    // ...
  }

  PatientStudies readStudies(PatientStudies parent, int vfLimit) {
    while (rb.index < vfLimit) {
      Study study = readStudy(parent, vfLimit);
      UID uid = study.lookup(Tag.StudyInstanceUID).value;
      parent.studies[uid] = study;
      //TODO maybe read date, time, etc. here if evalMode == eager
    }
    return parent;
  }

  Study readStudy(PatientStudies parent, int vfLimit) {
    return readItem(new Study(parent));
  }

  Study readSeriesSequenceItems(Study study, int vfLimit) {
    while (rb.index < vfLimit) {
      Series series = readOneSeries(study, vfLimit);
      UID uid = series.lookup(Tag.SeriesInstanceUID).value;
      //TODO maybe read date, time, etc. here if evalMode == eager
      study.series[uid] = series;
    }
    return study;
  }

  Series readOneSeries(Study study, vfLimit) {
    Series series = new Series(study);
    readItem(series);
    UID uid = series.lookup(Tag.SeriesInstanceUID).value;
    study.series[uid] = series;
    return series;
  }

  Series readInstanceSequenceItems(Series series, int vfLimit) {
    int bytesRead = rb.index;
    while (rb.index < vfLimit) {
      Instance instance = new Instance(series);
      bytesRead += readItem(instance);
      UID uid = instance.lookup(Tag.SOPInstanceUID).value;
      series.instances[uid] = instance;
    }
    if (bytesRead != vfLimit) parseError('vfLimit=$vfLimit, bytesRead=$bytesRead');
    return series;
  }

  /// [Read] the DICOM datasets contained in a [Sequence] [Item].
  Sequence readItems(Sequence sequence, int vfLimit) {
    while (rb.index < vfLimit)
      sequence.items.add(readItem(sequence, vfLimit));
    return sequence;
  }

  /// Read the [ITEM_START_TAG] and [length] and return [length].
  int readItemLength() {
    int tag = readTag();
    if (tag != ITEM_START_TAG) parseError("wrong tag in readItem");
    int vfLength = rb.uInt32();
    return vfLength;
  }
  /// Read the dataset contained in an [Item] into the [Dataset] [ds].  Returns the
  /// lengthInBytes of the [Dataset] read.
  int readItem(Dataset ds) {
    int vfLength = readItemLength();
    if (vfLength != -1) {
      readDataset(ds, getVFLimit(vfLength));
    } else {
      readDelimitedItem(ds);
    }
    return vfLength;
  }

  /// Read attributes from the [ReadBuffer] into the [Dataset] [ds].
  /// Any leading [Item] header has already been read.
  Dataset readDataset(Dataset ds, int vfLimit) {
    var value;
    while (rb.index < vfLimit) {
      int tag = readTag();
      VR vr = readVR();
      int vfLength = readVFLength(vr.isLarge);
      int vfLimit = getVFLimit(vfLength);
      if (vr == VR.SQ) {
        if (vfLength != -1) {
          value = readSequence(ds, tag, vfLimit);
        } else {
          value = readDelimitedSequence(ds, tag);
        }
      } else {
        value = readSimpleValue(tag, vr, vfLimit);
      }
      ds.add(tag, vr, value);
    }
    return ds;
  }

/*
  //Convert this to read study, series, instance, etc. sequences
  /**
        * Read Sequence
        *
        * A Sequence is a [list] of [Items] where each [Item] is a [Dataset].
        */
  /// [Read] the [ByteBuf] and store the results in [this] [Sequence].
  Object readPatientStudiesSequence(int tag, VR vr, int vfLength) {
    //int limit = rb.localLimit(vfLength);
    int vfLimit = getVFLimit(vfLength);
    switch (tag) {
      case PatientStudiesSequence:
        PatientStudies ps = new PatientStudies(null);
        ps.fmi = readFileMetaInfo(ps, vfLimit);
        ps.patient = readPatient(ps, vfLimit);
        readStudies(ps, vfLimit);
        return ps;
      case SeriesSequence:
      case PerSeriesFunctionalGroupsSequence:
        Dataset fmi = readItem(parent);
        Dataset dataset = readItem();
        var uid = dataset.lookupTag(Tag.SeriesInstanceUID.tag).value;
        if (!(uid is UID)) parseError("$uid should be a UID");
        return new Series(currentStudy, fmi, uid);
      case InstanceSequence:
      case PerInstanceFunctionalGroupsSequence:
        Dataset fmi = readItem();
        Dataset dataset = readItem();
        UID uid = dataset.lookupTag(Tag.SOPInstanceUID).value;
        return new Instance(currentSeries, fmi, uid);
      default:
        Sequence seq = new Sequence(tag, vr, vfLength);
        while (rb.isNotEmpty) seq.items.add(readItem(currentDataset));
        return seq;
    }
  }
  */

  void readAttribute(Dataset ds) {
    int tag = readTag();
    //*print(tagToHex(tag));
    VR vr = readVR();
    //*print('VR=$vr');
    int vfLength = readVFLength(vr.isLarge);
    //*print('vfLength=$len');
    var value = (vr == VR.SQ) ? readSequence(ds, tag, vfLength)
                              : readSimpleValue(tag, vr, vfLength);
    printAttribute(tag, vr, value);
    ds.add(tag, vr, value);
  }

  //TODO create a Union type instead of Object
  readSimpleValue(tag, vr, vfLength) {
    //if (evalMode == EvalMode.LAZY) return readBytes(vfLength);
    //This table is  ordered by tag frequency with most frequent first.
    //TODO some of these fields are returning strings instead of parsed values,
    //     e.g. TM, DA, DT,...
    switch (vr) {
      // The VRs in this list are in order of frequency of occurance - most to least.
      //TODO Any of these methods that might have a padding char have to change their lengths.
      case VR.DS:  // Decimal String
        return rb.readDecimalList(vfLength, maxItemLength: vr.maxLength);
      case VR.TM:  // Time
        //TODO readTime(vfLength);
        return rb.readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.IS:  // Integer String
        return rb.readIntegerList(vfLength, maxItemLength: vr.maxLength);
      case VR.UI:  // UID
        return rb.readUidList(vfLength, maxItemLength: vr.maxLength);
      case VR.BR:  // Bulkdata Reference
        return readBDReference(vr, vfLength);
      case VR.US:  // Unsigned Short
        return rb.uInt16List(vfLength);
      case VR.FD:  // Floating Point Double
        return rb.float64List(vfLength);
      case VR.CS:  // Code String
        return rb.readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.DT:  // DateTime
        //TODO readDateTime(vfLength)
        return rb.readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.SS:  // Signed Short
        return rb.int16List(vfLength);
      // VR.SQ should be here by frequency, but it is handled in readSequence.
      case VR.LT:  // Long Text
        return rb.readString(vfLength);
      //TODO should this be here?
      //case VR.UN:  // Unknown
      //  return readOtherBytes(vfLength);
      case VR.LO:  // Long String - maxLength = 64
        return rb.readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.SH:  // Short String - maxLength = 16
        return (tag == 0x00080201)
                 // Special handling for TimezoneOffsetFromUTC tag.
                 ? readTimezone(TimezoneOffsetFromUTC: true)
                 : rb.readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.UL:  // Unsigned Long
        return rb.uInt32List(vfLength);
      case VR.DA:  // Date
        return rb.readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.PN:  // Person Name
        return readPersonNameList(vfLength, maxItemLength: vr.maxLength);
      case VR.ST:  // Short Text - maxLength = 1024
        return rb.readString(vfLength);
      case VR.SL:  // Signed Long
        return rb.int32List(vfLength);
      case VR.FL:  // Floating Point Single
        return rb.float32List(vfLength);
      case VR.UR:  // URI/URL
        return rb.readUri(vfLength);
      case VR.AE:  // AE Title - maxLength = 16
        return rb.readStringList(vfLength, maxItemLength: vr.maxLength);
      // probably no attributes of the types below
      case VR.AS:  // Age String - Length must be 4
        return readAgeString(vfLength);
      case VR.AT:  // Attribute Tag
        return readTagList(vfLength);
      case VR.UT:  // Unlimited Text - maxLength = (2**32)-2
        return rb.readString(vfLength);
      case VR.OB:  // Other Byte
        return readOtherBytes(vfLength);
      case VR.OW:  // Other Word
        return readOtherWords(vfLength);
      case VR.OF:  // Other Float
        return readOtherFloats(vfLength);
      case VR.OD:  // Other Double
        return readOtherDoubles(vfLength);
      case VR.UN:  // Unknown
        return readOtherBytes(vfLength);
      case VR.SQ:
        //TODO should probably throw
        return parseError('vr=SQ in readSimpleAttribute');
      default:
        parseError("in readValueField");
    }
  }

  //Person Names are currently unparsed strings
  List<String> readPersonNameList(int vfLength, {maxItemLength: 64}) {
    List list = new List();
    while (rb.index < vfLength) {
      list.add(readPersonName(maxItemLength));
    }
    return list;
  }

  String readPersonName(int length) {
    return rb.readString(length);
    //TODO do we want to parse it now?
    //return PersonName.parse(bb.readString(length));
  }

  BDReference readBDReference(VR vr, int vfLength) {
    // VR.BD has only one value with four fields
    int offset = rb.uInt32();
    int length = rb.uInt32();
    // The first 3 fields contain 10 bytes
    Uri uri = rb.readUri(vfLength - 10);
    return new BDReference(vr, offset, length, uri);
  }

  Uint32List readTagList(vfLength) {
    int length = rb.checkSize(vfLength, Uint32List.BYTES_PER_ELEMENT);
    Uint32List list = new Uint32List(length);
    for (var i = 0; i < length; i++) list[i] = readTag();
    return list;
  }

  //TODO this should be checking that vfLength
  String readAgeString(vfLength) {
    if (vfLength != 4) parseError('VR=AS age string length != 4');
    return rb.readString(vfLength);
  }
  Uint8List readOtherBytes(int vfLength) {
    if (vfLength == -1) vfLength = findItemDelimiter();
    return rb.uInt8List(vfLength);
  }

  Uint16List readOtherWords(int vfLength) {
    if (vfLength == -1) vfLength = findItemDelimiter();
    return rb.uInt16List(vfLength);
  }

  Float32List readOtherFloats(int vfLength) {
    if (vfLength == -1) vfLength = findItemDelimiter();
    return rb.float32List(vfLength);
  }

  Float64List readOtherDoubles(int vfLength) {
    if (vfLength == -1) vfLength = findItemDelimiter();
    return rb.float64List(vfLength);
  }

  /// DICOM dates have 4 to 8 characters: YYYYMMDD, YYYYMM, or YYYY.
  //TODO Does not check for the correct number of days in month
  //TODO Does not remove trailing spaces
  //TODO The Min and Max years should be defined in System? and should be setable for
  //     the buffer.
  dynamic readDate(int vfLength, bool parse) {
    if ((vfLength < 4) || (vfLength > 8)) parseError('bad Date');
    int y, m, d;
    if (parse) {
      if (vfLength >= 8) y = rb.readPositiveInteger(4, 1980, 2020);
      if (vfLength >= 6) m = rb.readPositiveInteger(2, 1, 12);
      if (vfLength == 8) d = rb.readPositiveInteger(2, 1, maxDaysInMonth(y, m));
      return new DcmDate(y, m, d);
    } else {
      return rb.readString(8, 0);
    }
  }

  void _checkRange(int min, int val, int max) {
    if ((min > val) && (val > max))
      parseError('value out of range $min <= $val <= $max');
  }

  _readHour(vfLength) {

  }
  //TODO clean this up -
  /// DICOM Times use a 24 hour clock and are in the format HHMMSS.FFFFFF.  The
  /// maximum field size is 16 bytes.  Trailing spaces are allowed.  Only the [hour]
  /// field is required.
  DateTime readDcmTime(int vfLength) {
    _checkRange(2, vfLength, 16);
    int vfLimit = getVFLimit(vfLength);
    int h = rb.readPositiveInteger(2, 0, 23);
    if (rb.atLimit(vfLimit) return time;
    if (rb.localLimit(vfLimit)
    int m = rb.readPositiveInteger(2, 0, 59);
    int s = rb.readPositiveInteger(2, 0, 59);
    //TODO if needed - if (!rb.atLocalLimit(vfLimit))
    int ms = rb.readFractionalPart(7);
    Duration tzOffset = readTimezone();;
    var dateTime = new DateTime(0, 0, 0, h, m, s, ms);
    return dateTime.add(tzOffset)
  }

  //Note: DICOM timezone tag is (0008,0201) TimezoneOffsetFromUTC, VR=SH.
  //      If this tag appears in study all dates and times are in this timezone
  Duration readTimezoneOffset() {
    //The DICOM Timezone is alway 5 characters long [+,-]hhmm
    int sign, hours, minutes;
    int char = rb.uInt8();
    if (char == Ascii.MINUS) {
      sign = -1;
    } else if (char == Ascii.PLUS) {
      sign = 1;
    }
    //TODO
    hours = sign * rb.readPositiveInteger(2, 0, 23);
    minutes = sign * rb.readPositiveInteger(2, 0, 59);
    return new Duration(hours: hours, minutes: minutes);
  }

  DateTime readDateTime(int nBytes) {
    int y = rb.readPositiveInteger(4, 1970, System.start.year);
    int m = rb.readPositiveInteger(2, 1, 12);
    int d = rb.readPositiveInteger(2, 1, 31);
    int h = rb.readPositiveInteger(2, 0, 23);
    int min = rb.readPositiveInteger(2, 0, 59);
    int s = rb.readPositiveInteger(2, 0, 60);
    int char = rb.peek();
    int f = 0;
    Timezone tz;
    if (char == Ascii.PERIOD) {
      rb.skip(1);
      int f = rb.readPositiveInteger(7);
      char = rb.peek();
      if ((char == Ascii.PLUS) || (char == Ascii.MINUS)) {
        tz = readTimezone();
      }
    } else if ((char == Ascii.PLUS) || (char == Ascii.MINUS)) {
      tz = readTimezone();
    }
    DateTime dt = new DateTime(y, m, d, min, s, f);
    Duration tzDuration = tz.duration;
    return dt.add(tzDuration);
  }

  //***** Sequences and Items of _undefined length_  *****

  // Sequence and Item delimiters
  // Note: the 4 byte vfLength following the Delimiter tags has a value of 0x00000000.
  static const int ITEM_START_TAG = 0xFFFEE000;
  static const int ITEM_END_TAG = 0xFFFE000D; // Item Delimitation Tag
  static const int SEQUENCE_END_TAG = 0xFFFE00DD; // Sequence Delimitation Tag
  static const int DELIMITER_GROUP = 0xFFFF;
  static const int SEQUENCE_DELIMITER_ELEMENT = 0x00DD;
  static const int ITEM_DELIMITER_ELEMENT = 0x000D;

  ///Used to check for Sequence delimiters.
  bool isSequenceEnd() => _isDelimitedEnd(SEQUENCE_DELIMITER_ELEMENT);
  bool isNotSequenceEnd() => isSequenceEnd == false;

  bool isItemEnd() => _isDelimitedEnd(ITEM_DELIMITER_ELEMENT);
  bool isNotItemEnd() => isItemEnd == false;

  bool _isDelimitedEnd(int delimiter) {
    if (rb.peek16() != DELIMITER_GROUP) return false;
    if (rb.peek16() != delimiter) return false;
    rb.skip(4);
    //TODO remove the next two lines and make the line above skip(8)
    var len = rb.int32();
    if (len != 0) parseError("in checkForItemDelimiter");
    return true;
  }

  int findSequenceDelimiter() =>
      _findDelimiter(SEQUENCE_DELIMITER_ELEMENT);

  int findItemDelimiter() =>
      _findDelimiter(ITEM_DELIMITER_ELEMENT);

  int _findDelimiter(int delimiter) {
    int i;
    for (var i = rb.index; i < rb.limit; i++) {
      if (rb.peek16() != DELIMITER_GROUP) continue;
      if (rb.peek16() != delimiter) continue;
    }
    if (i >= rb.limit) parseError('No delimiter=$delimiter found!');
    return i - rb.index;
  }

  Sequence readDelimitedSequence(Dataset ds, int tag) {
    int limit = findSequenceDelimiter();
    Sequence seq = readSequence(ds, tag, limit - rb.index);
    int endTag = readTag();
    if (endTag != SEQUENCE_END_TAG) parseError('in Delimited Sequence');
    int vfLength = rb.uInt32();
    if (vfLength != 0) parseError('Sequence END length field not zero');
    return seq;
  }

  dynamic readDelimitedItem(Dataset ds) {
    int limit = findItemDelimiter();
    readDataset(ds, limit);
    int endTag = readTag();
    if (endTag != ITEM_END_TAG) parseError('in Delimited Item');
    int vfLength = rb.uInt32();
    if (vfLength != 0) parseError('Item END length field not zero');
    return ds;
  }

  Dataset readDelimitedDataset(Dataset parent) {
    //The parent is on the top of Dataset.stack
    Dataset ds = new Dataset(parent);
    //TODO isNotItemEnd could be a getter
    while (isNotItemEnd()) readAttribute(ds);
    return ds;
  }
}
