// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library eval_mode;

class EvalMode {
  final String value;

  const EvalMode(this.value);

  static const EvalMode LAZY = const EvalMode('Lazy');
  static const EvalMode EAGER = const EvalMode('Eager');
  static const EvalMode VALIDATE = const EvalMode('Validate');

  toString() => 'Enum.EvalMode.$value';
}
