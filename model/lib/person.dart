// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library person;


class Person {
  //TODO finish
  PersonName  name;
  DateTime    dateOfBirth;
  Sex         sex;
  //TODO add address parser and address type
  String     address;

  //TODO need to add validators for the fields, e.g. DOB should not be in future
  //     and should not be more than 110 years ago.
  Person(this.name, this.dateOfBirth, this.sex, this.address);

}

/**
 * DICOM Person Name (VR = PN)
 *
 * Each component of the PersonName has a maximum of 64 characters.  It cannot include "\".
 * The components are separated by the character "^".
 *
 * PersonNames as [String] has a canonical form, which is:
 *    "family^given^middle^prefix^suffix".
 *
 */
class PersonName {
  String familyName;
  String givenName;
  String middleName;
  String prefix;
  String suffix;

  PersonName(this.familyName, this.givenName, this.middleName, this.prefix, this.suffix);

  PersonName.empty() : this("", "", "", "", "");

  PersonName.list(List<String> l) : this(l[0], l[1], l[2], l[3], l[4]);

  /// Parses a DICOM format string into a [PersonName] and returns it.
  static parse(String s) {
    PersonName pn = new PersonName.empty();
    Exception error() {
      throw new Exception("Person Name component too long/");
    }
    List<String>names = new List(5);
    int start = 0;
    int end;
    for(var i = 0; i < 5; i++) {
      var end = s.indexOf("^", start);
      if (end == -1) {
        end = s.length;
        if ((end - start) > 64) error();
        names[i] = s.substring(start, end);
        return new PersonName.list(names);
      } else {
        if ((end - start) > 64) error();
        names[i] = s.substring(start, end);
        start = end + 1;
      }
    }
  }

  String toDcmString() => "$familyName^$givenName^$middleName^$prefix^$suffix";

  String toString() => "$prefix $givenName $middleName $familyName $suffix";
}

class Sex {
  final String value;

  const Sex(this.value);

  static const FEMALE = const Sex('Female');
  static const MALE = const Sex('Male');

  bool get isMale => (value == 'Male');
  bool get isFemale => !isMale;
  toAbbreviation() => (value == 'Male') ? 'M' : 'F';

  toString() => '$value';
}

class Address {
  String street1;
  String street2;
  String city;
  String state;
  String zipCode;
}